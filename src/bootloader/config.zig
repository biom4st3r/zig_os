//! Config file for the bootloader
//! 2023 by Samuel Fiedler

pub const debug = true;
pub const print_elf_info = true;
pub const print_progheaders = false;
pub const print_usable_progheaders = true;
